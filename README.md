# ContextuWall Add-on for VANTED

The ContextuWall Add-on for VANTED provides an easy way to use VANTED with the [ContextuWall framework](http://www.immersiveanalytics.org/contextuwall).

## License
Copyright Monash University, 2014-2016 licensed under the [GPLv2 License](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html).

## System Requirements
The ContextuWall Add-on requires a [VANTED](http://www.vanted.org/) installation.

## Installation
*  Download the [jar](https://bitbucket.org/tczauderna/contextuwall-addon/downloads/contextuwall.jar).
*  Start VANTED
	*  Drag'n'Drop the jar on VANTED, or
	*  Go to Edit -> Preferences -> Addon Manager and install the Add-on

## Configuration

*  Start VANTED
	*  Go to Edit -> Preferences -> Addon Manager and click `Show Preferences Folder`
	*  Go to the `addons` folder and edit the `contextuwall.properties` file
	*  Example configuration  
	
		(replace `displayname` with the name of your display, e.g. `myDisplay`)
	
		    displayname.columns = ###       // number of screens in horizontal direction  
		    displayname.rows = ###          // number of screens in vertical direction  
		    displayname.screenwidth = ###   // horizontal resolution of a single screen  
		    displayname.screenheight = ###  // vertical resolution of a single screen  
		    displayname.url = ###           // url of the ContextuWall server  
		    displayname.port = ###          // port of the ContextuWall server  
		    displayname.password = ###      // password for the ContextuWall server  

## Contact

*  Email me at tobias.czauderna@monash.edu