/**
 * Copyright (c) 2014-2016 Monash University, Australia
 */
package org.vanted.addons.contextuwall;

import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import de.ipk_gatersleben.ag_nw.graffiti.plugins.misc.svg_exporter.SizeSettingZoom;

/**
 * @author Tobias Czauderna
 */
@SuppressWarnings("nls")
public class CWComboBoxModels {
	
	private ComboBoxModel<String> zoomComboBoxModel; // zoom combo box model
	private ComboBoxModel<String> columnsComboBoxModel; // columns combo box model
	private ComboBoxModel<String> rowsComboBoxModel; // rows combo box model
	
	/**
	 * Create zoom, columns, and rows combo box models depending on the display parameters.
	 * 
	 * @param display
	 */
	public CWComboBoxModels(CWDisplay display) {
		
		// zoom combo box model
		Vector<String> zoom = new Vector<>();
		zoom.add("N/A");
		for (SizeSettingZoom sizeSettingZoom : SizeSettingZoom.values())
			if (sizeSettingZoom.equals(SizeSettingZoom.LVIEW))
				zoom.add("Current");
			else
				zoom.add(sizeSettingZoom.toString());
		this.zoomComboBoxModel = new DefaultComboBoxModel<>(zoom);
		this.zoomComboBoxModel.setSelectedItem(SizeSettingZoom.L100.toString());
		
		// columns and rows combo box model
		this.columnsComboBoxModel = createComboBoxModelInt(display.getMaxColumns());
		this.rowsComboBoxModel = createComboBoxModelInt(display.getMaxRows());
		
	}
	
	/**
	 * Create combo box model for columns and rows.
	 * 
	 * @param maxValue
	 * @return default combo box model
	 */
	private static DefaultComboBoxModel<String> createComboBoxModelInt(int maxValue) {
		
		Vector<String> values = new Vector<>();
		values.add("N/A");
		for (int k = 1; k <= maxValue; k++)
			values.add(String.valueOf(k));
		return new DefaultComboBoxModel<>(values);
		
	}
	
	/**
	 * Get zoom combo box model.
	 * 
	 * @return zoom combo box model
	 */
	public ComboBoxModel<String> getZoomComboBoxModel() {
		
		return this.zoomComboBoxModel;
		
	}
	
	/**
	 * Get columns combo box model.
	 * 
	 * @return columns combo box model
	 */
	public ComboBoxModel<String> getColumnsComboBoxModel() {
		
		return this.columnsComboBoxModel;
		
	}
	
	/**
	 * Get rows combo box model.
	 * 
	 * @return rows combo box model
	 */
	public ComboBoxModel<String> getRowsComboBoxModel() {
		
		return this.rowsComboBoxModel;
		
	}
	
}
