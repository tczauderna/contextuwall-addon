/**
 * Copyright (c) 2014-2016 Monash University, Australia
 */
package org.vanted.addons.contextuwall;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.ErrorMsg;
import org.ReleaseInfo;
import org.apache.log4j.Logger;
import org.graffiti.plugin.inspector.InspectorTab;
import org.vanted.addons.contextuwall.socketio.CWSocketIOClient;

import de.ipk_gatersleben.ag_nw.graffiti.plugins.addons.AddonAdapter;

/**
 * @author Tobias Czauderna
 */
/*
 * TODO: get displays from an updated properties file
 * TODO: implement progress bar to show the progress of image upload
 * TODO: implement update button for sending updates on images
 * TODO: use a table-like gui element to select image position instead of combo boxes
 * TODO: Can CWSocketIOClient be reused (instead of creating a new instance on "Connect")?
 */
@SuppressWarnings("nls")
public class ContextuWall extends AddonAdapter {
	
	private static CWDisplay[] displays;
	private static int numberOfDisplays = 0;
	private static int currentDisplay = -1;
	
	private static CWSocketIOClient[] cwSocketIOClients;
	
	private static String tempFolderName; // temporary folder for image files
	
	private static final Logger logger = Logger.getLogger(ContextuWall.class);
	
	// /**
	// *
	// */
	// @Override
	// public ImageIcon getIcon() {
	//
	// ClassLoader classLoader = this.getClass().getClassLoader();
	// String path = this.getClass().getPackage().getName().replace('.', '/');
	// URL url = classLoader.getResource(path + "/contextuwall.png");
	// if (url != null)
	// return new ImageIcon(url);
	// return super.getIcon();
	//
	// }
	
	/**
	 * Initialise the add-on.
	 */
	@Override
	protected void initializeAddon() {
		
		// get displays from properties file
		Properties properties = new Properties();
		String addonsFolderName = ReleaseInfo.getAppSubdirFolderWithFinalSep("addons");
		String fileName = getAddonName().toLowerCase() + ".properties";
		// write initial properties file if it doesn't exist
		if (!new File(addonsFolderName + fileName).exists())
			writeInitialPropertiesFile(properties, addonsFolderName, fileName);
		// read from existing properties file
		else
			readPropertiesFile(properties, addonsFolderName, fileName);
		
		// initialise currentDisplay
		initialiseCurrentDisplay();
		
		// empty temporary folder, this folder stores images which have been sent to a display server in a former session
		tempFolderName = ReleaseInfo.getAppSubdirFolderWithFinalSep("Temp" + getAddonName());
		emptyTemporaryFolder();
		
		this.tabs = new InspectorTab[] { new ContextuWallTab() };
		
	}
	
	/**
	 * Get the add-on name.
	 * 
	 * @return Add-on name
	 */
	public static String getAddonName() {
		
		return "ContextuWall";
		
	}
	
	/**
	 * @param properties
	 * @param addonsFolderName
	 * @param fileName
	 */
	private static void writeInitialPropertiesFile(Properties properties, String addonsFolderName, String fileName) {
		
		try (OutputStream outputStream = new FileOutputStream(addonsFolderName + fileName)) {
			properties.store(outputStream, getAddonName() + " display configurations\n" +
					"displayname.columns = ###\ndisplayname.rows = ###\n" +
					"displayname.screenwidth = ###\ndisplayname.screenheight = ###\n" +
					"displayname.url = ###\ndisplayname.port = ###\ndisplayname.password =");
		} catch (IOException ioException) {
			ErrorMsg.addErrorMessage(ioException);
			logger.info(ioException.getMessage());
		}
		
	}
	
	/**
	 * @param properties
	 * @param addonsFolderName
	 * @param fileName
	 */
	public static void readPropertiesFile(Properties properties, String addonsFolderName, String fileName) {
		
		int numberOfProperties = 7;
		
		try (InputStream inputStream = new FileInputStream(addonsFolderName + fileName)) {
			properties.load(inputStream);
		} catch (IOException ioException) {
			ErrorMsg.addErrorMessage(ioException);
			logger.info(ioException.getMessage());
		}
		// for each display six properties (columns, rows, screen width, screen height, url, port) should have been defined
		if (properties.size() > 0 && properties.size() % numberOfProperties == 0) {
			// get display names from the properties
			Set<String> setDisplayNames = new HashSet<>();
			for (String string : properties.stringPropertyNames()) {
				String[] arrString = string.split("\\.");
				setDisplayNames.add(arrString[0]);
			}
			// sort display names alphabetically
			List<String> displayNames = new ArrayList<>(setDisplayNames);
			Collections.sort(displayNames);
			// initialise numberOfDisplays
			numberOfDisplays = properties.size() / numberOfProperties;
			// define displays
			displays = new CWDisplay[numberOfDisplays];
			int k = 0;
			for (String displayName : displayNames) {
				int columns = Integer.parseInt(properties.getProperty(displayName + ".columns", "0"));
				int rows = Integer.parseInt(properties.getProperty(displayName + ".rows", "0"));
				int screenWidth = Integer.parseInt(properties.getProperty(displayName + ".screenwidth", "0"));
				int screenHeight = Integer.parseInt(properties.getProperty(displayName + ".screenheight", "0"));
				String url = properties.getProperty(displayName + ".url", "");
				String port = properties.getProperty(displayName + ".port", "");
				String password = properties.getProperty(displayName + ".password", "");
				if (columns > 0 && rows > 0 && screenWidth > 0 && screenHeight > 0 && url.length() > 0 && port.length() > 0) {
					displays[k] = new CWDisplay(displayName, columns, rows, screenWidth, screenHeight, url, port, password);
					k++;
				}
			}
			// reinitialise numberOfDisplays
			if (k < numberOfDisplays)
				numberOfDisplays = k;
			// initialise cwSocketIOClients
			if (numberOfDisplays > 0)
				cwSocketIOClients = new CWSocketIOClient[numberOfDisplays];
		}
		
	}
	
	/**
	 * Empty temporary folder.
	 */
	private static void emptyTemporaryFolder() {
		
		File folder = new File(tempFolderName);
		if (folder.exists() && folder.isDirectory()) {
			File[] files = folder.listFiles();
			if (files != null && files.length > 0) {
				boolean isError = false;
				for (File file : files)
					if (!file.delete()) {
						isError = true;
						logger.info("Could not delete file " + file.getName() + " from temporary folder " + tempFolderName);
					}
				if (!isError)
					logger.info("Temporary folder " + tempFolderName + " emptied");
			}
		}
		else
			logger.info("Could not find temporary folder " + tempFolderName);
		
	}
	
	/**
	 * @return
	 */
	public static int getNumberOfDisplays() {
		
		return numberOfDisplays;
		
	}
	
	/**
	 * 
	 */
	public static void initialiseCurrentDisplay() {
		
		if (numberOfDisplays > 0)
			currentDisplay = 0;
		
	}
	
	/**
	 * Set current display.
	 * 
	 * @param display
	 */
	public static void setCurrentDisplay(int display) {
		
		currentDisplay = display;
		
	}
	
	/**
	 * Get current display.
	 * 
	 * @return current display
	 */
	public static int getCurrentDisplay() {
		
		return currentDisplay;
		
	}
	
	/**
	 * Get display.
	 * 
	 * @param display
	 * @return
	 */
	public static CWDisplay getDisplay(int display) {
		
		return displays[display];
		
	}
	
	/**
	 * Set socketIO client for current display.
	 */
	public static void setCWSocketIOClient() {
		
		CWDisplay cwDisplay = displays[currentDisplay];
		try {
			cwSocketIOClients[currentDisplay] = new CWSocketIOClient(cwDisplay.getURL(), cwDisplay.getPort());
		} catch (URISyntaxException uriSyntaxException) {
			ErrorMsg.addErrorMessage(uriSyntaxException);
		}
		
	}
	
	/**
	 * Get socketIO client for current display.
	 * 
	 * @return socketIO client for current display
	 */
	public static CWSocketIOClient getCWSocketIOClient() {
		
		return cwSocketIOClients[currentDisplay];
		
	}
	
	/**
	 * Get temporary folder name.
	 *
	 * @return temporary folder name
	 */
	public static String getTempFolderName() {
		
		return tempFolderName;
		
	}
	
}
