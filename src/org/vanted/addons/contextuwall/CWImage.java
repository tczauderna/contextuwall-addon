/**
 * Copyright (c) 2014-2016 Monash University, Australia
 */
package org.vanted.addons.contextuwall;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

import org.AttributeHelper;
import org.ErrorMsg;
import org.Vector2d;
import org.apache.log4j.Logger;
import org.graffiti.attributes.CollectionAttribute;
import org.graffiti.editor.MainFrame;
import org.graffiti.graph.AdjListGraph;
import org.graffiti.graph.Edge;
import org.graffiti.graph.Graph;
import org.graffiti.graph.GraphElement;
import org.graffiti.graph.Node;
import org.graffiti.plugin.view.GraphElementComponent;
import org.graffiti.plugin.view.View;
import org.graffiti.selection.Selection;
import org.json.JSONException;
import org.json.JSONObject;
import org.vanted.addons.contextuwall.socketio.CWSocketIOClient;

import de.ipk_gatersleben.ag_nw.graffiti.plugins.layouters.graph_to_origin_mover.CenterLayouterAlgorithm;
import de.ipk_gatersleben.ag_nw.graffiti.plugins.misc.svg_exporter.PngJpegAlgorithm;
import de.ipk_gatersleben.ag_nw.graffiti.plugins.misc.svg_exporter.PngJpegAlgorithmParams;
import de.ipk_gatersleben.ag_nw.graffiti.plugins.misc.svg_exporter.SizeSetting;
import de.ipk_gatersleben.ag_nw.graffiti.plugins.misc.svg_exporter.SizeSettingZoom;

/**
 * @author Tobias Czauderna
 */
@SuppressWarnings("nls")
public class CWImage {
	
	static boolean ack;
	
	static final Logger logger = Logger.getLogger(CWImage.class);
	
	/**
	 * Save image as png file.
	 * 
	 * @param graph
	 * @param selection
	 * @param folderName
	 * @param imgFileName
	 * @return status - 0 if image could be saved as png file, -1 if image could not be saved as png file
	 */
	public static int saveImageFile(Graph graph, Selection selection, String folderName, String imgFileName) {
		
		Graph workingGraph;
		Rectangle viewRectangle; // view rectangle to calculate image size
		int border; // image border
		View view = MainFrame.getInstance().getActiveSession().getActiveView();
		Dimension imgSize; // image size
		
		// if nodes and edges are selected only this selection is saved as png
		// therefore a new graph is created (but not shown, except for debugging)
		// nodes and edges from the selection are copied to this new graph
		if (selection != null && selection.getElements().size() > 0) {
			// create new graph and copy all graph attributes to new graph
			// this is important if the graph contains diagrams
			workingGraph = new AdjListGraph((CollectionAttribute) graph.getAttributes().copy());
			// a mapping from selected nodes in the original graph to nodes in the new graph is necessary for the edges
			Map<Node, Node> mapNode2NodeCopy = new HashMap<>();
			for (Node node : selection.getNodes()) {
				Node nodeCopy = workingGraph.addNodeCopy(node);
				mapNode2NodeCopy.put(node, nodeCopy);
			}
			for (Edge edge : selection.getEdges()) {
				// get corresponding nodes for source and target in the new graph to make a copy of a selected edge
				Node srcNodeCopy = mapNode2NodeCopy.get(edge.getSource());
				Node tgtNodeCopy = mapNode2NodeCopy.get(edge.getTarget());
				if (srcNodeCopy != null && tgtNodeCopy != null)
					workingGraph.addEdgeCopy(edge, srcNodeCopy, tgtNodeCopy);
			}
			viewRectangle = getViewRectangle(view, selection.getElements(), selection.getNodes());
			border = Math.min(getImageBorder(viewRectangle), 50);
			imgSize = new Dimension(viewRectangle.width + 2 * border, viewRectangle.height + 2 * border);
			// move graph top-left
			CenterLayouterAlgorithm.moveGraph(workingGraph, "", true, border, border);
			// for debugging
			// SwingUtilities.invokeLater(new Runnable() {
			//
			// @Override
			// public void run() {
			//
			// MainFrame.getInstance().showGraph(graph, null);
			//
			// }
			// });
		}
		else {
			workingGraph = graph;
			viewRectangle = getViewRectangle(view, workingGraph.getGraphElements(), workingGraph.getNodes());
			border = getImageBorder(viewRectangle);
			if (viewRectangle.x < viewRectangle.y)
				imgSize = new Dimension(viewRectangle.width + 2 * border, viewRectangle.height + viewRectangle.y + border);
			else
				imgSize = new Dimension(viewRectangle.width + viewRectangle.x + border, viewRectangle.height + 2 * border);
		}
		
		// set and check image parameters
		int zoom = ContextuWallTab.getZoom(); // zoom parameter from GUI
		int columns = ContextuWallTab.getColumns(); // columns parameter from GUI
		int rows = ContextuWallTab.getRows(); // rows parameter from GUI
		CWDisplay display = ContextuWall.getDisplay(ContextuWall.getCurrentDisplay());
		int screenWidth = display.getScreenWidth(); // screen width depending on current display
		int screenHeight = display.getScreenHeight(); // screen height depending on current display
		double scalingFactor = 1.0; // scaling factor for the image
		PngJpegAlgorithmParams pngJpegAlgorithmParams = new PngJpegAlgorithmParams();
		pngJpegAlgorithmParams.setIncludeTooltip(false); // tool tips are only useful for web pages
		// zoom parameter set
		if (zoom > 0) {
			setImageParameters(pngJpegAlgorithmParams, -1, -1, true, 800, SizeSetting.ZOOM, SizeSettingZoom.values()[zoom - 1], border);
			double zoomingFactor = 1.0; // zooming factor of the view, needs to be used if zoom parameter is set to 'current'
			Graphics2D graphics2D = (Graphics2D) view.getViewComponent().getGraphics();
			if (graphics2D != null)
				zoomingFactor = graphics2D.getTransform().getScaleX();
			scalingFactor = (SizeSettingZoom.values()[zoom - 1]).getScale(zoomingFactor);
		}
		// columns parameter set
		if (columns > 0) {
			setImageParameters(pngJpegAlgorithmParams, -1, screenWidth * columns, true, screenWidth * columns, SizeSetting.FIXED, SizeSettingZoom.L100, border);
			scalingFactor = screenWidth * columns / (double) imgSize.width;
		}
		// rows parameter set
		if (rows > 0) {
			setImageParameters(pngJpegAlgorithmParams, screenHeight * rows, -1, true, screenHeight * rows, SizeSetting.FIXED, SizeSettingZoom.L100, border);
			scalingFactor = screenHeight * rows / (double) imgSize.height;
		}
		int option = JOptionPane.YES_OPTION;
		// use the scaling factor to check whether the image fits to the screen
		// issue warning if the image will be too large
		if ((screenWidth * display.getMaxColumns() < imgSize.width * scalingFactor) ||
				(screenHeight * display.getMaxRows() < imgSize.height * scalingFactor)) {
			String text = String.format("The resulting image will be too large for the screen.\n" +
					"Screen size: %d x %d\n" + "Image size (100%%): %d x %d\n" + "Image size: %d x %d\n" +
					"Scaling factor: %.2f\n\n" + "Do you want to continue?\n\n",
					new Integer(screenWidth * display.getMaxColumns()), new Integer(screenHeight * display.getMaxRows()),
					new Integer(imgSize.width), new Integer(imgSize.height),
					new Integer((int) (imgSize.width * scalingFactor)), new Integer((int) (imgSize.height * scalingFactor)),
					new Double(scalingFactor));
			option = JOptionPane.showConfirmDialog(MainFrame.getInstance(), text, "Warning", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
		}
		
		if (option != JOptionPane.YES_OPTION)
			return 1;
		else
			if (option == JOptionPane.YES_OPTION) {
				// save image as png
				PngJpegAlgorithm.createPNGimageFromGraph(workingGraph, folderName + imgFileName, pngJpegAlgorithmParams);
				if (!(new File(folderName + imgFileName)).exists()) {
					logger.info("Could not save image as png");
					return -1;
				}
			}
		return 0;
		
	}
	
	/**
	 * Set image parameters.
	 * 
	 * @param pngJpegAlgorithmParams
	 * @param maxHeight
	 * @param maxWidth
	 * @param scaleFixedUseWidth
	 * @param scaleFixedUseWidthOrHeightValue
	 * @param sizeSetting
	 * @param sizeSettingZoom
	 * @param border
	 */
	private static void setImageParameters(PngJpegAlgorithmParams pngJpegAlgorithmParams, int maxHeight, int maxWidth, boolean scaleFixedUseWidth,
			int scaleFixedUseWidthOrHeightValue, SizeSetting sizeSetting, SizeSettingZoom sizeSettingZoom, int border) {
		
		pngJpegAlgorithmParams.setMaxHeight(maxHeight);
		pngJpegAlgorithmParams.setMaxWidth(maxWidth);
		pngJpegAlgorithmParams.setScaleFixedUseWidth(scaleFixedUseWidth);
		pngJpegAlgorithmParams.setScaleFixedUseWidthOrHeightValue(scaleFixedUseWidthOrHeightValue);
		pngJpegAlgorithmParams.setScaleSetting(sizeSetting);
		pngJpegAlgorithmParams.setScaleZoomSetting(sizeSettingZoom);
		pngJpegAlgorithmParams.setBorderWidth(border);
		
	}
	
	/**
	 * Get view rectangle.
	 * 
	 * @param view
	 * @param graphElements
	 * @param nodes
	 * @return view rectangle
	 */
	private static Rectangle getViewRectangle(View view, Collection<GraphElement> graphElements, Collection<Node> nodes) {
		
		if (nodes != null && nodes.iterator().hasNext()) {
			// initialise the viewRectangle with the position of an arbitrary node
			Vector2d position = AttributeHelper.getPositionVec2d(nodes.iterator().next());
			Rectangle viewRectangle = new Rectangle((int) position.x, (int) position.y, 0, 0);
			for (GraphElement graphElement : graphElements) {
				if (AttributeHelper.isHiddenGraphElement(graphElement))
					continue;
				GraphElementComponent graphElementComponent = view.getComponentForElement(graphElement);
				if (graphElementComponent == null)
					continue;
				viewRectangle.add(graphElementComponent.getBounds());
				for (Object object : graphElementComponent.getAttributeComponents())
					if (object instanceof JComponent) {
						JComponent jComponent = (JComponent) object;
						if (jComponent.isVisible())
							viewRectangle.add(jComponent.getBounds());
					}
			}
			return viewRectangle;
		}
		return new Rectangle(0, 0);
		
	}
	
	/**
	 * Get image border from view rectangle.
	 * 
	 * @param viewRectangle
	 * @return border
	 */
	private static int getImageBorder(Rectangle viewRectangle) {
		
		if (viewRectangle.x < viewRectangle.y)
			return viewRectangle.x;
		return viewRectangle.y;
		
	}
	
	/**
	 * Send the image as JSON object.
	 * 
	 * @param cwSocketIOClient
	 * @param folderName
	 * @param imgFileName
	 * @param formatName
	 * @param hints
	 * @param normLocX
	 * @param normLocY
	 * @param displayHeight
	 * @return
	 */
	public static int sendImage(CWSocketIOClient cwSocketIOClient, String folderName, String imgFileName, String formatName, int hints,
			double normLocX, double normLocY, int displayHeight) {
		
		// create image base64 string
		File fileImage = new File(folderName + imgFileName);
		ByteArrayOutputStream baosImage = new ByteArrayOutputStream(Math.min((int) fileImage.length(), Integer.MAX_VALUE));
		Path path = Paths.get(folderName, imgFileName);
		long numberOfBytes = 0;
		try {
			numberOfBytes = Files.copy(path, baosImage);
		} catch (IOException ioException) {
			ErrorMsg.addErrorMessage(ioException);
			logger.info(ioException.getMessage());
		}
		if (numberOfBytes > 0) {
			
			String strImage = Base64.getEncoder().encodeToString(baosImage.toByteArray());
			if (strImage != null && strImage.length() > 0) {
				// create thumbnail base64 string
				BufferedImage bimgImage = null;
				try {
					bimgImage = ImageIO.read(fileImage);
				} catch (IOException ioException) {
					ErrorMsg.addErrorMessage(ioException);
					logger.info(ioException.getMessage());
				}
				String strThumbnail = "";
				if (bimgImage != null) {
					// strThumbnail == null or strThumbnail == "" works, server takes care of it
					strThumbnail = createThumbnailBase64String(bimgImage, formatName, hints);
					// scale depending on image height and display height
					double scale = (double) bimgImage.getHeight() / (double) displayHeight;
					// create JSON object normalised location
					JSONObject jsonObjNormLoc = createJSONObjectNormLoc(normLocX, normLocY);
					// create JSON object image
					JSONObject jsonObjImage = createJSONObjectImage(imgFileName, strThumbnail, jsonObjNormLoc, bimgImage.getWidth(), bimgImage.getHeight(), scale);
					// send incoming image notify to server
					// only useful when strThumbnail != null and strThumbnail != ""
					if (strThumbnail != null && strThumbnail.length() > 0)
						cwSocketIOClient.sendThumbnail(jsonObjImage);
					// add the image to the JSON object
					try {
						jsonObjImage.put("m_image64", strImage);
					} catch (JSONException jsonException) {
						ErrorMsg.addErrorMessage(jsonException);
						logger.info(jsonException.getMessage());
					}
					// send incoming image to server
					cwSocketIOClient.sendImage(jsonObjImage);
					return 0;
				}
			}
		}
		logger.info("Could not send image: image data could not be created");
		return -1;
		
	}
	
	/**
	 * Create thumbnail for the image.
	 * 
	 * @param bimgImage
	 * @param formatName
	 * @param hints
	 * @return String
	 */
	private static String createThumbnailBase64String(BufferedImage bimgImage, String formatName, int hints) {
		
		// create thumbnail image
		double width = bimgImage.getWidth();
		double height = bimgImage.getHeight();
		if (width > height && width > 256) {
			height = Math.round(height / width * 256.0);
			width = 256;
		}
		else {
			width = Math.round(width / height * 256.0);
			height = 256;
		}
		Image imgThumbnail = bimgImage.getScaledInstance((int) width, (int) height, hints);
		BufferedImage bimgThumbnail = new BufferedImage((int) width, (int) height, bimgImage.getType());
		bimgThumbnail.getGraphics().drawImage(imgThumbnail, 0, 0, null);
		bimgThumbnail.getGraphics().dispose();
		// create thumbnail base64 string
		ByteArrayOutputStream baosThumbnail = new ByteArrayOutputStream();
		boolean hasWriter = true;
		try {
			hasWriter = ImageIO.write(bimgThumbnail, formatName, baosThumbnail);
		} catch (IOException ioException) {
			ErrorMsg.addErrorMessage(ioException);
			logger.info(ioException.getMessage());
			return null;
		}
		if (!hasWriter)
			return "";
		return Base64.getEncoder().encodeToString(baosThumbnail.toByteArray());
		
	}
	
	/**
	 * Create JSON object for the image.
	 * 
	 * @param strFileName
	 * @param strThumbnail
	 * @param jsonObjNormLoc
	 * @param width
	 * @param height
	 * @param scale
	 * @return JSONObject
	 */
	private static JSONObject createJSONObjectImage(String strFileName, String strThumbnail, JSONObject jsonObjNormLoc,
			int width, int height, double scale) {
		
		JSONObject jsonObjImage = new JSONObject();
		try {
			jsonObjImage.put("filename", strFileName);
			jsonObjImage.put("m_thumbnail64", strThumbnail);
			jsonObjImage.put("m_normalisedLocation", jsonObjNormLoc);
			jsonObjImage.put("m_width", width);
			jsonObjImage.put("m_height", height);
			jsonObjImage.put("m_scale", scale);
		} catch (JSONException jsonException) {
			ErrorMsg.addErrorMessage(jsonException);
			logger.info(jsonException.getMessage());
		}
		return jsonObjImage;
		
	}
	
	/**
	 * Create JSON object for the normalised image location.
	 * 
	 * @param x
	 * @param y
	 * @return JSONObject
	 */
	private static JSONObject createJSONObjectNormLoc(double x, double y) {
		
		JSONObject jsonObjNormLoc = new JSONObject();
		try {
			jsonObjNormLoc.put("X", x);
			jsonObjNormLoc.put("Y", y);
		} catch (JSONException jsonException) {
			ErrorMsg.addErrorMessage(jsonException);
			logger.info(jsonException.getMessage());
		}
		return jsonObjNormLoc;
		
	}
	
}
