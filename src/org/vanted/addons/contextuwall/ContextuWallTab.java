/**
 * Copyright (c) 2014-2016 Monash University, Australia
 */
package org.vanted.addons.contextuwall;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstants;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;

import org.ErrorMsg;
import org.graffiti.editor.MainFrame;
import org.graffiti.graph.Graph;
import org.graffiti.plugin.inspector.InspectorTab;
import org.graffiti.plugin.view.View;
import org.graffiti.selection.Selection;
import org.graffiti.selection.SelectionEvent;
import org.graffiti.selection.SelectionListener;
import org.graffiti.session.EditorSession;
import org.vanted.addons.contextuwall.socketio.CWSocketIOClient;

import de.ipk_gatersleben.ag_nw.graffiti.plugins.misc.svg_exporter.SizeSettingZoom;

/**
 * @author Tobias Czauderna
 */
/*
 * TODO: implement button to get displays from an updated properties file
 * 1) ContextuWall.readPropertiesFile
 * 2) ContextuWall.initialiseCurrentDisplay
 * 3) initialise all ComboBoxModels
 * 4) enable displays combo box
 * 5) set selected index displays combo box
 * 6) setModels
 * 7) updateGUI
 * TODO: implement button for sending updates on images
 */
@SuppressWarnings("nls")
public class ContextuWallTab extends InspectorTab implements SelectionListener {
	
	private static final long serialVersionUID = 1L;
	
	Selection selection = null;
	
	static Map<Integer, CWComboBoxModels> mapComboBoxModels = new HashMap<>(); // map for displays and associated combo boxes
	
	static JButton jConnectButton;
	static JButton jDisconnectButton;
	static JButton jReconnectButton;
	static JButton jClearWallButton;
	
	static JComboBox<String> jcboxDisplays; // displays combo box
	static JLabel jConnectionInfo;
	
	static JComboBox<String> jcboxZoom; // zoom combo box
	static boolean isZoomChange = false;
	static JComboBox<String> jcboxColumns; // columns combo box
	static boolean isColumnChange = false;
	static JComboBox<String> jcboxRows; // rows combo box
	static boolean isRowChange = false;
	
	static JButton jSendImageButton;
	
	/**
	 * Set up ContextuWall tab.
	 */
	public ContextuWallTab() {
		
		int numberOfDisplays = ContextuWall.getNumberOfDisplays();
		int currentDisplay = ContextuWall.getCurrentDisplay();
		
		// sizes for table layout
		double[] columns = new double[] { TableLayoutConstants.FILL };
		double[] rows = new double[] { TableLayoutConstants.PREFERRED, // connect / disconnect buttons
				TableLayoutConstants.PREFERRED, 20, // reconnect / clear wall buttons
				TableLayoutConstants.PREFERRED, 5, // displays combo box
				TableLayoutConstants.PREFERRED, 20, // connection info
				TableLayoutConstants.PREFERRED, 10, // zoom / columns / rows combo boxes
				TableLayoutConstants.PREFERRED, 20, // read me
				TableLayoutConstants.PREFERRED // send image button
		};
		double[][] size = new double[][] { columns, rows };
		
		setLayout(new TableLayout(size));
		setBackground(Color.WHITE);
		
		// create connect / disconnect buttons
		jConnectButton = new JButton("Connect");
		jConnectButton.setEnabled(false);
		jConnectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				ContextuWall.setCWSocketIOClient();
				CWSocketIOClient cwSocketIOClient = ContextuWall.getCWSocketIOClient();
				if (cwSocketIOClient != null && !cwSocketIOClient.isConnected()) {
					cwSocketIOClient.openConnection();
					updateGUI();
				}
			}
		});
		jDisconnectButton = new JButton("Disconnect");
		jDisconnectButton.setEnabled(false);
		jDisconnectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				CWSocketIOClient cwSocketIOClient = ContextuWall.getCWSocketIOClient();
				if (cwSocketIOClient != null && cwSocketIOClient.isConnected()) {
					cwSocketIOClient.closeConnection();
					updateGUI();
				}
			}
		});
		add(TableLayout.getSplit(jConnectButton, jDisconnectButton, 0.5, 0.5), "0,0");
		
		// reconnect / clear wall buttons
		jReconnectButton = new JButton("Reconnect");
		jReconnectButton.setEnabled(false);
		jReconnectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				CWSocketIOClient cwSocketIOClient = ContextuWall.getCWSocketIOClient();
				if (cwSocketIOClient != null && cwSocketIOClient.isConnected()) {
					cwSocketIOClient.closeConnection();
					cwSocketIOClient.openConnection();
					updateGUI();
				}
			}
		});
		jClearWallButton = new JButton("Clear Wall");
		jClearWallButton.setEnabled(false);
		jClearWallButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				CWSocketIOClient cwSocketIOClient = ContextuWall.getCWSocketIOClient();
				if (cwSocketIOClient != null && cwSocketIOClient.isConnected())
					cwSocketIOClient.clearWall();
			}
		});
		add(TableLayout.getSplit(jReconnectButton, jClearWallButton, 0.5, 0.5), "0,1");
		
		// displays combo box
		jcboxDisplays = new JComboBox<>();
		// displays combo box model
		Vector<String> displayNames = new Vector<>();
		for (int k = 0; k < numberOfDisplays; k++) {
			CWDisplay display = ContextuWall.getDisplay(k);
			displayNames.add(display.getName() + " (" + display.getURL() + ":" + display.getPort() + ")");
		}
		DefaultComboBoxModel<String> cboxmodelDisplays = new DefaultComboBoxModel<>(displayNames);
		jcboxDisplays.setModel(cboxmodelDisplays);
		// to set the index the combo box model has to be set
		jcboxDisplays.setSelectedIndex(currentDisplay);
		// to add the action listener the index should already be set
		// if the index is set after the action listener has been added
		// the action listener will be called and an exception will occur since
		// the zoom / columns / rows combo boxes have not been initialised yet
		jcboxDisplays.addActionListener(displaysActionListener());
		if (currentDisplay == -1)
			jcboxDisplays.setEnabled(false);
		add(TableLayout.get3Split(new JLabel(), jcboxDisplays, new JLabel(), 0.05, 0.9, 0.05), "0,3");
		
		// create connection info label
		jConnectionInfo = new JLabel();
		add(TableLayout.get3Split(new JLabel(), jConnectionInfo, new JLabel(), 0.05, 0.9, 0.05), "0,5");
		
		// create zoom / columns / rows combo boxes
		ArrayList<JComponent> jComponents = new ArrayList<>();
		jcboxZoom = createComboBox(zoomActionListener());
		jcboxZoom.setEnabled(false);
		jComponents.add(new JLabel("<html>&nbsp;Zoom:"));
		jComponents.add(jcboxZoom);
		jcboxColumns = createComboBox(columnsActionListener());
		jcboxColumns.setEnabled(false);
		jComponents.add(new JLabel("<html>&nbsp;Columns:"));
		jComponents.add(jcboxColumns);
		jcboxRows = createComboBox(rowsActionListener());
		jcboxRows.setEnabled(false);
		jComponents.add(new JLabel("<html>&nbsp;Rows:"));
		jComponents.add(jcboxRows);
		// create combo box models
		for (int k = 0; k < numberOfDisplays; k++)
			mapComboBoxModels.put(new Integer(k), new CWComboBoxModels(ContextuWall.getDisplay(k)));
		if (currentDisplay != -1)
			setModels(mapComboBoxModels.get(new Integer(currentDisplay)));
		add(TableLayout.getMultiSplit(jComponents), "0,7");
		
		// create read me label
		JLabel jReadMe = new JLabel("<html><p align=\"justify\">When nodes and edges are selected only this selection is considered during image creation.");
		add(TableLayout.get3Split(new JLabel(), jReadMe, new JLabel(), 0.05, 0.9, 0.05), "0,9");
		
		// create send image button
		jSendImageButton = new JButton("Send Image");
		jSendImageButton.setEnabled(false);
		jSendImageButton.addActionListener(sendImageActionListener());
		add(TableLayout.get3Split(new JLabel(), jSendImageButton, new JLabel(), 0.2, 0.6, 0.2), "0,11");
		
		if (currentDisplay != -1)
			updateGUI();
		
		validate();
		
	}
	
	/**
	 * 
	 */
	@Override
	public String getName() {
		
		return getTitle();
		
	}
	
	/**
	 * 
	 */
	@Override
	public String getTitle() {
		
		return ContextuWall.getAddonName();
		
	}
	
	/**
	 * 
	 */
	@Override
	public boolean visibleForView(View v) {
		
		return true;
		
	}
	
	/**
	 * Create combo box.
	 * 
	 * @param actionListener
	 * @return combo box
	 */
	private static JComboBox<String> createComboBox(ActionListener actionListener) {
		
		JComboBox<String> jComboBox = new JComboBox<>();
		jComboBox.addActionListener(actionListener);
		return jComboBox;
		
	}
	
	/**
	 * @param comboBoxModels
	 */
	static void setModels(CWComboBoxModels comboBoxModels) {
		
		jcboxZoom.setModel(comboBoxModels.getZoomComboBoxModel());
		jcboxColumns.setModel(comboBoxModels.getColumnsComboBoxModel());
		jcboxRows.setModel(comboBoxModels.getRowsComboBoxModel());
		
	}
	
	/**
	 * Action listener for displays combo box.
	 * 
	 * @return action listener
	 */
	private static ActionListener displaysActionListener() {
		
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				
				int currentDisplay = jcboxDisplays.getSelectedIndex();
				ContextuWall.setCurrentDisplay(currentDisplay);
				setModels(mapComboBoxModels.get(new Integer(currentDisplay)));
				updateGUI();
				
			}
			
		};
		
	}
	
	/**
	 * Action listener for zoom combo box.
	 * 
	 * @return action listener
	 */
	private static ActionListener zoomActionListener() {
		
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				
				// if columns and rows is already set to 'N/A', zoom cannot be set to 'N/A' as well
				// check and set to default '100%'
				if (getColumns() == 0 && getRows() == 0 && getZoom() == 0)
					jcboxZoom.setSelectedItem(SizeSettingZoom.L100.toString());
				else
					// if zoom is set manually by the user, set columns and rows to 'N/A'
					// otherwise ignore the event
					if (!isColumnChange && !isRowChange) {
						isZoomChange = true;
						jcboxColumns.setSelectedIndex(0);
						jcboxRows.setSelectedIndex(0);
						isZoomChange = false;
					}
				
			}
			
		};
		
	}
	
	/**
	 * Action listener for columns combo box.
	 * 
	 * @return action listener
	 */
	private static ActionListener columnsActionListener() {
		
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				
				// if columns is set manually by the user, set zoom and rows to 'N/A'
				// otherwise ignore the event
				if (!isZoomChange && !isRowChange) {
					isColumnChange = true;
					jcboxZoom.setSelectedIndex(0);
					jcboxRows.setSelectedIndex(0);
					isColumnChange = false;
				}
				
			}
			
		};
		
	}
	
	/**
	 * Action listener for rows combo box.
	 * 
	 * @return action listener
	 */
	private static ActionListener rowsActionListener() {
		
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				
				// if rows is set manually by the user, set zoom and columns to 'N/A'
				// otherwise ignore the event
				if (!isZoomChange && !isColumnChange) {
					isRowChange = true;
					jcboxZoom.setSelectedIndex(0);
					jcboxColumns.setSelectedIndex(0);
					isRowChange = false;
				}
				
			}
			
		};
		
	}
	
	/**
	 * @return action listener
	 */
	private ActionListener sendImageActionListener() {
		
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				
				int currentDisplay = ContextuWall.getCurrentDisplay();
				CWDisplay display = ContextuWall.getDisplay(currentDisplay);
				CWSocketIOClient cwSocketIOClient = ContextuWall.getCWSocketIOClient();
				Graph graph = null;
				EditorSession editorSession = MainFrame.getInstance().getActiveEditorSession();
				if (editorSession != null)
					graph = editorSession.getGraph();
				if (graph != null)
					if (cwSocketIOClient != null && cwSocketIOClient.isConnected()) {
						String folderName = ContextuWall.getTempFolderName();
						// String imgFileName = String.valueOf(System.currentTimeMillis()) + "#" + cwSocketIOClient.getID() + ".png";
						String imgFileName = UUID.randomUUID().toString() + ".png";
						int saveStatus = CWImage.saveImageFile(graph, ContextuWallTab.this.selection, folderName, imgFileName);
						if (saveStatus == -1)
							ErrorMsg.addErrorMessage("Could not send image: could not save image as png");
						else
							if (saveStatus == 0) {
								int displayHeight = display.getMaxRows() * display.getScreenHeight();
								// int sendStatus = CWImage.sendImage(cwSocketIOClient.getSocket(), folderName, imgFileName, "png", Image.SCALE_FAST,
								// 0.5, 0.5, displayHeight);
								int sendStatus = CWImage.sendImage(cwSocketIOClient, folderName, imgFileName, "png", Image.SCALE_FAST, 0.5, 0.5, displayHeight);
								if (sendStatus == -1)
									ErrorMsg.addErrorMessage("Could not send image: image data could not be created");
							}
					}
					else
						ErrorMsg.addErrorMessage("Could not send image: no connection to server available");
				
			}
			
		};
		
	}
	
	/**
	 * Get selected index for zoom combo box.
	 * 
	 * @return selected index
	 */
	public static int getZoom() {
		
		return jcboxZoom.getSelectedIndex();
		
	}
	
	/**
	 * Get selected index for columns combo box.
	 * 
	 * @return selected index
	 */
	public static int getColumns() {
		
		return jcboxColumns.getSelectedIndex();
		
	}
	
	/**
	 * Get selected index for rows combo box.
	 * 
	 * @return selected index
	 */
	public static int getRows() {
		
		return jcboxRows.getSelectedIndex();
		
	}
	
	/**
	 * 
	 */
	public static void updateGUI() {
		
		int currentDisplay = ContextuWall.getCurrentDisplay();
		CWDisplay display = ContextuWall.getDisplay(currentDisplay);
		CWSocketIOClient cwSocketIOClient = ContextuWall.getCWSocketIOClient();
		if (cwSocketIOClient != null && cwSocketIOClient.isConnected()) {
			jConnectButton.setEnabled(false);
			jDisconnectButton.setEnabled(true);
			jReconnectButton.setEnabled(true);
			jClearWallButton.setEnabled(true);
			jConnectionInfo.setText("Connected to " + display.getName());
			jConnectionInfo.setForeground(new Color(0, 220, 0));
			jcboxZoom.setEnabled(true);
			jcboxColumns.setEnabled(true);
			jcboxRows.setEnabled(true);
			jSendImageButton.setEnabled(true);
		}
		else {
			jConnectButton.setEnabled(true);
			jDisconnectButton.setEnabled(false);
			jReconnectButton.setEnabled(false);
			jClearWallButton.setEnabled(false);
			jConnectionInfo.setText("Not connected to " + display.getName());
			jConnectionInfo.setForeground(new Color(220, 0, 0));
			jcboxZoom.setEnabled(false);
			jcboxColumns.setEnabled(false);
			jcboxRows.setEnabled(false);
			jSendImageButton.setEnabled(false);
		}
		
	}
	
	/**
	 * 
	 */
	@Override
	public void selectionChanged(SelectionEvent e) {
		
		this.selection = e.getSelection();
		
	}
	
	/**
	 * 
	 */
	@Override
	public void selectionListChanged(SelectionEvent e) {
		
		// no action required yet
		
	}
	
}
