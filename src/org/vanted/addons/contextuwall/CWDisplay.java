/**
 * Copyright (c) 2014-2016 Monash University, Australia
 */
package org.vanted.addons.contextuwall;

/**
 * @author Tobias Czauderna
 */
public class CWDisplay {
	
	private String name; // display name
	private int maxColumns; // max columns
	private int maxRows; // max rows
	private int screenWidth; // screen width
	private int screenHeight; // screen height
	private String url; // display server url
	private String port; // display server port
	private String password; // display server password
	
	/**
	 * Create a new display and store name, max columns, max rows, screen width, and screen height, display server url and port.
	 * 
	 * @param name
	 * @param maxColumns
	 * @param maxRows
	 * @param screenWidth
	 * @param screenHeight
	 * @param url
	 * @param port
	 * @param password
	 */
	public CWDisplay(String name, int maxColumns, int maxRows, int screenWidth, int screenHeight, String url, String port, String password) {
		
		this.name = name;
		this.maxColumns = maxColumns;
		this.maxRows = maxRows;
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
		this.url = url;
		this.port = port;
		this.password = password;
		
	}
	
	/**
	 * Get display name.
	 *
	 * @return display name
	 */
	public String getName() {
		
		return this.name;
		
	}
	
	/**
	 * Get max columns for the display.
	 * 
	 * @return max columns
	 */
	public int getMaxColumns() {
		
		return this.maxColumns;
		
	}
	
	/**
	 * Get max rows for display.
	 * 
	 * @return max rows
	 */
	public int getMaxRows() {
		
		return this.maxRows;
		
	}
	
	/**
	 * Get width of one screen of the display.
	 * 
	 * @return screen width
	 */
	public int getScreenWidth() {
		
		return this.screenWidth;
		
	}
	
	/**
	 * Get height of one screen of the display.
	 * 
	 * @return screen height
	 */
	public int getScreenHeight() {
		
		return this.screenHeight;
		
	}
	
	/**
	 * Get display server url.
	 * 
	 * @return display server url
	 */
	public String getURL() {
		
		return this.url;
		
	}
	
	/**
	 * Get display server port.
	 * 
	 * @return display server port
	 */
	public String getPort() {
		
		return this.port;
		
	}
	
	/**
	 * Get display server password.
	 * 
	 * @return display server password
	 */
	public String getPassword() {
		
		return this.password;
		
	}
	
}
