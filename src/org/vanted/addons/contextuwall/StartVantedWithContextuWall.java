/**
 * Copyright (c) 2014-2016 Monash University, Australia
 */
package org.vanted.addons.contextuwall;

import de.ipk_gatersleben.ag_nw.graffiti.plugins.gui.webstart.Main;

/**
 * @author Tobias Czauderna
 */
@SuppressWarnings("nls")
public class StartVantedWithContextuWall {
	
	/**
	 * Get the add-on name and add the file extension '.xml'.
	 * 
	 * @return Add-on name plus file extension '.xml'.
	 */
	public static String getAddonNameXML() {
		
		return ContextuWall.getAddonName() + ".xml";
		
	}
	
	/**
	 * Start VANTED with the ContextuWall Add-on.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		System.out.println("Starting VANTED with " + ContextuWall.getAddonName());
		Main.startVanted(args, getAddonNameXML());
		
	}
	
}
