/**
 * Copyright (c) 2014-2016 Monash University, Australia
 */
package org.vanted.addons.contextuwall.socketio;

/**
 * @author Tobias Czauderna
 */
@SuppressWarnings("nls")
public class CWSocketIOEvents {
	
	public static final String AUTHENTICATE = "authenticate";
	public static final String AUTHENTICATE_OK = "authenticate_ok";
	public static final String INCOMINGIMAGE = "incomingImage";
	public static final String INCOMINGIMAGENOTIFY = "incomingImageNotify";
	public static final String INCOMINGDISPLAYSPECS = "incomingDisplaySpecs";
	public static final String REMOVINGDISPLAYSPECS = "removingDisplaySpecs";
	public static final String CLEARWALL = "clearWall";
	
}
