/**
 * Copyright (c) 2014-2016 Monash University, Australia
 */
package org.vanted.addons.contextuwall.socketio;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.client.SocketIOException;
import io.socket.emitter.Emitter.Listener;
import io.socket.engineio.client.EngineIOException;

import java.net.URISyntaxException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Logger;

/**
 * @author Tobias Czauderna
 */
@SuppressWarnings("nls")
public abstract class SocketIOClient {
	
	Socket socket = null;
	
	String url;
	String port;
	private IO.Options ioOptions;
	boolean isError;
	Map<String, Listener> mapListeners = new HashMap<>();
	
	static final Logger logger = Logger.getLogger(SocketIOClient.class);
	
	/**
	 * @param url
	 * @param port
	 * @throws URISyntaxException
	 */
	public SocketIOClient(String url, String port) throws URISyntaxException {
		
		this.url = url;
		this.port = port;
		this.ioOptions = new IO.Options();
		this.ioOptions.reconnection = false;
		
		if (url.startsWith("https")) {
			this.ioOptions.secure = true;
			// SSLContext and HostnameVerifier are required for https connections
			SSLContext sslContext = null;
			try {
				sslContext = SSLContext.getInstance("SSL");
				sslContext.init(null, new TrustManager[] {
						
						new X509TrustManager() {
							
							@Override
							public X509Certificate[] getAcceptedIssuers() {
								return null;
							}
							
							@Override
							public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
								// TODO Auto-generated method stub
							}
							
							@Override
							public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
								// TODO Auto-generated method stub
							}
							
						}
				
				}, null);
			} catch (Exception exception) {
				logger.info(exception.getMessage());
			}
			this.ioOptions.sslContext = sslContext;
			this.ioOptions.hostnameVerifier = new HostnameVerifier() {
				
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
				
			};
		}
		
		this.socket = IO.socket(this.url + ":" + this.port, this.ioOptions);
		
	}
	
	/**
	 * 
	 */
	public void openConnection() {
		
		if (this.socket != null) {
			initDefaultSocket();
			this.isError = false;
			// in comparison to the C# library we need to call socket.open or socket.connect
			// use socket.open, socket.connect calls socket.open
			this.socket.open();
			logger.info("Connecting ...");
			// wait until connected or error has occurred
			while (!this.socket.connected() && !this.isError)
				sleep(100);
			if (!this.socket.connected())
				resetSocket();
		}
		
	}
	
	/**
	 * 
	 */
	protected void initDefaultSocket() {
		
		Listener listener;
		
		listener = new Listener() {
			@Override
			public void call(Object... args) {
				logger.info("Connected to " + SocketIOClient.this.url + ":" + SocketIOClient.this.port + ", socket id: " + SocketIOClient.this.socket.id());
			}
		};
		this.socket.on(Socket.EVENT_CONNECT, listener);
		this.mapListeners.put(Socket.EVENT_CONNECT, listener);
		
		listener = new Listener() {
			@Override
			public void call(Object... args) {
				SocketIOClient.this.isError = true;
				logger.info("Connection error (Socket.EVENT_CONNECT_ERROR)");
				evaluateError(args[0]);
			}
		};
		this.socket.on(Socket.EVENT_CONNECT_ERROR, listener);
		this.mapListeners.put(Socket.EVENT_CONNECT_ERROR, listener);
		
		listener = new Listener() {
			@Override
			public void call(Object... args) {
				SocketIOClient.this.isError = true;
				logger.info("Connection timeout (Socket.EVENT_CONNECT_TIMEOUT)");
				evaluateError(args[0]);
			}
		};
		this.socket.on(Socket.EVENT_CONNECT_TIMEOUT, listener);
		this.mapListeners.put(Socket.EVENT_CONNECT_TIMEOUT, listener);
		
		listener = new Listener() {
			@Override
			public void call(Object... args) {
				resetSocket();
				cleanUp();
				logger.info("Disconnected from " + SocketIOClient.this.url + ":" + SocketIOClient.this.port + ": " + (String) args[0]);
			}
		};
		this.socket.on(Socket.EVENT_DISCONNECT, listener);
		this.mapListeners.put(Socket.EVENT_DISCONNECT, listener);
		
		listener = new Listener() {
			@Override
			public void call(Object... args) {
				logger.info("Connection error (Socket.EVENT_ERROR)");
				evaluateError(args[0]);
			}
		};
		this.socket.on(Socket.EVENT_ERROR, listener);
		this.mapListeners.put(Socket.EVENT_ERROR, listener);
		
		listener = new Listener() {
			@Override
			public void call(Object... args) {
				logger.info("Incoming message: " + (String) args[0]);
			}
		};
		this.socket.on(Socket.EVENT_MESSAGE, listener);
		this.mapListeners.put(Socket.EVENT_MESSAGE, listener);
		
	}
	
	/**
	 * 
	 */
	abstract void cleanUp();
	
	/**
	 * 
	 */
	public void closeConnection() {
		
		if (this.socket != null && this.socket.connected()) {
			// use socket.close, socket.disconnect calls socket.close
			this.socket.close();
			// wait until disconnected
			while (this.socket.connected())
				sleep(100);
		}
		
	}
	
	/**
	 * 
	 */
	void resetSocket() {
		
		for (String event : this.mapListeners.keySet())
			this.socket.off(event, this.mapListeners.get(event));
		
	}
	
	/**
	 * @return
	 */
	public Socket getSocket() {
		
		if (this.socket != null)
			return this.socket;
		return null;
		
	}
	
	/**
	 * @return String
	 */
	public String getID() {
		
		if (this.socket != null)
			return this.socket.id();
		return null;
		
	}
	
	/**
	 * @return boolean
	 */
	public boolean isConnected() {
		
		if (this.socket != null)
			return this.socket.connected();
		return false;
		
	}
	
	/**
	 * @param arg
	 */
	protected void evaluateError(Object arg) {
		
		Exception exception = null;
		String message = null;
		if (arg instanceof SocketIOException) {
			exception = (SocketIOException) arg;
			handleError(exception);
			logger.info("SocketIOException: " + exception.getMessage());
		}
		else
			if (arg instanceof EngineIOException) {
				exception = (EngineIOException) arg;
				handleError(exception);
				logger.info("EngineIOException: " + exception.getMessage());
			}
			else {
				message = (String) arg;
				handleError(message);
				logger.info(message);
			}
		
	}
	
	/**
	 * @param exception
	 */
	abstract void handleError(Exception exception);
	
	/**
	 * @param message
	 */
	abstract void handleError(String message);
	
	/**
	 * @param time
	 */
	protected static void sleep(long time) {
		
		try {
			Thread.sleep(time);
		} catch (InterruptedException interruptedException) {
			logger.info(interruptedException.getMessage());
		}
		
	}
	
}
