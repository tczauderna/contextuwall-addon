/**
 * Copyright (c) 2014-2016 Monash University, Australia
 */
package org.vanted.addons.contextuwall.socketio;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter.Listener;

import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.ErrorMsg;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.vanted.addons.contextuwall.CWDisplay;
import org.vanted.addons.contextuwall.ContextuWall;
import org.vanted.addons.contextuwall.ContextuWallTab;

/**
 * @author Tobias Czauderna
 */
@SuppressWarnings("nls")
public class CWSocketIOClient extends SocketIOClient {
	
	boolean ack;
	boolean doCleanUp;
	
	static final Logger _logger = Logger.getLogger(CWSocketIOClient.class);
	
	/**
	 * @param url
	 * @param port
	 * @throws URISyntaxException
	 */
	public CWSocketIOClient(String url, String port) throws URISyntaxException {
		
		super(url, port);
		this.doCleanUp = true;
		
	}
	
	/**
	 * @param url
	 * @param port
	 * @param doCleanUp
	 * @throws URISyntaxException
	 */
	public CWSocketIOClient(String url, String port, boolean doCleanUp) throws URISyntaxException {
		
		super(url, port);
		this.doCleanUp = doCleanUp;
		
	}
	
	/**
	 * 
	 */
	@Override
	public void openConnection() {
		
		if (this.socket != null) {
			initDefaultSocket();
			initContextuWallSocket();
			this.isError = false;
			// in comparison to the C# library we need to call socket.open or socket.connect
			// use socket.open, socket.connect calls socket.open
			this.socket.open();
			_logger.info("Connecting ...");
			// wait until connected or error has occurred
			while (!this.socket.connected() && !this.isError)
				sleep(100);
			if (!this.socket.connected())
				resetSocket();
			
		}
		
	}
	
	/**
	 * 
	 */
	private void initContextuWallSocket() {
		
		Listener listener;
		
		// remove listeners on Socket.EVENT_CONNECT from parent class SocketIOClient
		for (Listener _listener : this.socket.listeners(Socket.EVENT_CONNECT))
			if (_listener.getClass().getName().contains(this.getClass().getSuperclass().getName()))
				this.socket.off(Socket.EVENT_CONNECT, _listener);
		
		listener = new Listener() {
			@Override
			public void call(Object... args) {
				if (CWSocketIOClient.this.url.startsWith("https")) {
					JSONObject jsonObjPassword = new JSONObject();
					try {
						// String hash = getMd5Hash("foobar");
						// if (hash != null)
						// jsonObjPassword.put("password", hash);
						CWDisplay display = ContextuWall.getDisplay(ContextuWall.getCurrentDisplay());
						jsonObjPassword.put("password", display.getPassword());
					} catch (JSONException jsonException) {
						_logger.info(jsonException.getMessage());
					}
					sendPassword(jsonObjPassword);
					_logger.info("Authentification ...");
				}
				else
					_logger.info("Connected to " + CWSocketIOClient.this.url + ":" + CWSocketIOClient.this.port + ", socket id: " +
							CWSocketIOClient.this.socket.id());
			}
		};
		this.socket.on(Socket.EVENT_CONNECT, listener);
		this.mapListeners.put(Socket.EVENT_CONNECT, listener);
		
		listener = new Listener() {
			@Override
			public void call(Object... args) {
				_logger.info("Connected to " + CWSocketIOClient.this.url + ":" + CWSocketIOClient.this.port + ", socket id: " + CWSocketIOClient.this.socket.id());
			}
		};
		this.socket.on(CWSocketIOEvents.AUTHENTICATE_OK, listener);
		this.mapListeners.put(CWSocketIOEvents.AUTHENTICATE_OK, listener);
		
		listener = new Listener() {
			@Override
			public void call(Object... args) {
				JSONObject jsonObj = (JSONObject) args[0];
				try {
					_logger.info("Incoming display specs: " + (String) jsonObj.get("name") + " " + (String) jsonObj.get("width") + "x" +
							(String) jsonObj.get("height"));
				} catch (JSONException jsonException) {
					ErrorMsg.addErrorMessage(jsonException);
					_logger.info(jsonException.getMessage());
				}
			}
		};
		this.socket.on(CWSocketIOEvents.INCOMINGDISPLAYSPECS, listener);
		this.mapListeners.put(CWSocketIOEvents.INCOMINGDISPLAYSPECS, listener);
		
		listener = new Listener() {
			@Override
			public void call(Object... args) {
				_logger.info("Removing display specs: " + (String) args[0]);
			}
		};
		this.socket.on(CWSocketIOEvents.REMOVINGDISPLAYSPECS, listener);
		this.mapListeners.put(CWSocketIOEvents.REMOVINGDISPLAYSPECS, listener);
		
	}
	
	/**
	 * Create MD5 hash for password.
	 * 
	 * @param password
	 * @return MD5 hash for password
	 */
	static String getMd5Hash(String password) {
		
		try {
			StringBuffer stringBuffer = new StringBuffer();
			byte[] digest = MessageDigest.getInstance("MD5").digest(password.getBytes());
			for (int k = 0; k < digest.length; k++) {
				String hexString = Integer.toHexString(digest[k] & 0xFF);
				if (hexString.length() == 1)
					hexString = "0" + hexString;
				stringBuffer.append(hexString);
			}
			return stringBuffer.toString();
		} catch (NoSuchAlgorithmException noSuchAlgorithmException) {
			logger.info(noSuchAlgorithmException.getMessage());
		}
		return null;
		
	}
	
	/**
	 * 
	 */
	@Override
	void cleanUp() {
		
		if (this.doCleanUp)
			ContextuWallTab.updateGUI();
		
	}
	
	/**
	 * @param jsonObjPassword
	 */
	public void sendPassword(JSONObject jsonObjPassword) {
		
		if (this.socket != null && this.socket.connected()) {
			this.socket.emit(CWSocketIOEvents.AUTHENTICATE, jsonObjPassword);
			// this.ack = false;
			// this.socket.emit(CWSocketIOEvents.AUTHENTICATE, jsonObjPassword, new Ack() {
			// @Override
			// public void call(Object... args) {
			// CWSocketIOClient.this.ack = true;
			// _logger.info(CWSocketIOEvents.AUTHENTICATE + ": received by server");
			// }
			// });
			// while (!this.ack)
			// sleep(100);
		}
		
	}
	
	/**
	 * @param jsonObjThumbnail
	 */
	public void sendThumbnail(JSONObject jsonObjThumbnail) {
		
		if (this.socket != null && this.socket.connected()) {
			this.ack = false;
			this.socket.emit(CWSocketIOEvents.INCOMINGIMAGENOTIFY, jsonObjThumbnail, new Ack() {
				@Override
				public void call(Object... args) {
					CWSocketIOClient.this.ack = true;
					_logger.info(CWSocketIOEvents.INCOMINGIMAGENOTIFY + ": received by server");
				}
			});
			while (!this.ack)
				sleep(100);
		}
		
	}
	
	/**
	 * @param jsonObjImage
	 */
	public void sendImage(JSONObject jsonObjImage) {
		
		if (this.socket != null && this.socket.connected()) {
			this.ack = false;
			this.socket.emit(CWSocketIOEvents.INCOMINGIMAGE, jsonObjImage, new Ack() {
				@Override
				public void call(Object... args) {
					CWSocketIOClient.this.ack = true;
					_logger.info(CWSocketIOEvents.INCOMINGIMAGE + ": received by server");
				}
			});
			while (!this.ack)
				sleep(100);
		}
		
	}
	
	/**
	 * 
	 */
	public void clearWall() {
		
		if (this.socket != null && this.socket.connected()) {
			this.ack = false;
			this.socket.emit(CWSocketIOEvents.CLEARWALL, new Ack() {
				@Override
				public void call(Object... args) {
					CWSocketIOClient.this.ack = true;
					_logger.info(CWSocketIOEvents.CLEARWALL + ": received by server");
				}
			});
			while (!this.ack)
				sleep(100);
		}
		
	}
	
	/**
	 * 
	 */
	@Override
	void handleError(Exception exception) {
		
		ErrorMsg.addErrorMessage(exception);
		
	}
	
	/**
	 * 
	 */
	@Override
	void handleError(String message) {
		
		ErrorMsg.addErrorMessage(message);
		
	}
	
}
